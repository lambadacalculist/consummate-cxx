
* Clean up the rendering code; review how it suppresses line breaks
  and remove where appropriate

* Suppress const copy constructor if any fields are not copyable
* Suppress move constructor if any fields are not movable
* In fieldAccessors, create all valid accessors
* Fix internal representation of and detection of primitive types (isNumeric)
* Fix indentation of functions in pretty-printer
* Allow line break after return type in pretty-printer
* Autogenerate clone function
* Support templated generics
