
{-# LANGUAGE OverloadedStrings #-}
module Language.ConsummateCXX.CodeGeneration where

import Data.Text(Text)
import qualified Data.Text as Text

import Language.ConsummateCXX.ADT
import Language.ConsummateCXX.Internal.Syntax
import Language.ConsummateCXX.Internal.ReqType
import Language.ConsummateCXX.Predefined

mangleForMember :: Text -> Text
mangleForMember t = t <> "_"

mangleForParam :: Text -> Text
mangleForParam t = t

mangleGetter :: Text -> Text
mangleGetter t = "get_" <> t

mangleSetter :: Text -> Text
mangleSetter t = "set_" <> t

mangleTaker :: Text -> Text
mangleTaker t = "take_" <> t

manglePutter :: Text -> Text
manglePutter t = "put_" <> t

mangleReffer :: Text -> Text
mangleReffer t = "ref_" <> t

mangleCtor :: Text -> Text
mangleCtor t = "make_" <> t

mangleForFunctorType :: Text -> Text
mangleForFunctorType t = "F_" <> t

mangleForFunctor :: Text -> Text
mangleForFunctor t = "f_" <> t

-- | Name used for a temporary pointer variable in functions
tempPtrName :: QualID
tempPtrName = uname "p"

-- | Name used for a temporary other variable in constructors
tempOtherName :: QualID
tempOtherName = uname "other"

referenceType :: ADT -> Type
referenceType adt =
  rtType $ applySmartPointer (adtPointer adt) (unameType $ adtName adt)

-- | Produce forward declarations for one ADT
forwardDeclaration :: ADT -> [Decl]
forwardDeclaration = adtForwardDeclaration

-- | Produce C++ code to define an ADT
declaration :: ADT -> [Decl]
declaration adt@(ADT {adtNamespace = ns, adtName = adtName, adtPointer = ptr,
                      adtConstructors = ctors}) =
  let baseDecls = adtDeclaration adt
      ctorDecls = concat [constructorDeclaration adt c | c <- ctors]
      matchDecls = [matcher False True adt, matcher True True adt]
  in baseDecls ++ ctorDecls ++ matchDecls

-- | Parameters of a matcher function.
data MatcherParams =
  MatcherParams
  { mpFunctorTypes :: [QualID]
  , mpFunctors :: [QualID]
  }

-- | Create a declaration of a function to pattern match an object.
matcherParams :: Bool -- ^ Whether the object being matched is const
              -> ADT  -- ^ Data type to generate matcher for
              -> MatcherParams
matcherParams _ (ADT {adtConstructors = []}) =
  error "Cannot create matcher for ADT with no constructors"

matcherParams isConst (ADT {adtNamespace = ns, adtName = adtName, adtPointer = ptr,
                      adtConstructors = ctors}) =
  let tmplParams = [uname $ mangleForFunctorType $ conName c | c <- ctors]
      funParams = [uname $ mangleForFunctor $ conName c | c <- ctors]
  in MatcherParams tmplParams funParams  

-- | Declarations of matcher template parameters
matcherParamTypeDecls :: MatcherParams -> [VarDecl]
matcherParamTypeDecls mp = map typeVarDecl $ mpFunctorTypes mp

-- | Declarations of matcher parameters
matcherParamDecls :: MatcherParams -> [VarDecl]
matcherParamDecls mp =
  -- Makes code like `const F_Con1 &f_Con1`
  [varDecl p (refT $ constT $ idT t)
  | (p, t) <- zip (mpFunctors mp) (mpFunctorTypes mp)]

-- | Create a function to pattern match an object.
matcher :: Bool -- ^ Whether the object being matched is const
        -> Bool -- ^ Whether this is defining (versus declaring) the function
        -> ADT  -- ^ Data type to generate matcher for
        -> Decl -- ^ Declaration of matcher function
matcher _ _ (ADT {adtConstructors = []}) =
  error "Cannot create matcher for ADT with no constructors"

matcher isConst isDef adt@(ADT {adtNamespace = ns, adtName = adtName, adtPointer = ptr,
                      adtConstructors = ctors}) =
  let mp = matcherParams isConst adt
      constructorTypes = map (unameType . conName) ctors

      -- Makes code like `decltype(declval<F_Con1>()(declval<Con1>()))`
      funRType =
        let functor = declvalE (idT $ head $ mpFunctorTypes mp)
            inst = declvalE (myConstT $ unameType $ conName $ head ctors)
        in DeclType $ CallE functor [inst]

      matchStatement =
        ifElseS matchError
        [matchCase t p | (t, p) <- zip constructorTypes (mpFunctors mp)]

      matchError = ThrowS $ callE (idE $ qname [name "std"] "bad_cast") []
      matcherDecl =
        let
          -- Declared with unqualified name "match" in class body.
          -- Defined with qualified name "Class::match" at global scope.
          funName = if isDef
                    then qname [name adtName] "match"
                    else uname "match"
          funBody = if isDef
                    then Just [matchStatement]
                    else Nothing
          thisQualifiers = if isConst then ["const"] else []
        in FDecl $ FunDecl funName [] (matcherParamDecls mp) (Just funRType) thisQualifiers [] funBody
  in TDecl (TemplateDecl (matcherParamTypeDecls mp) matcherDecl)
  where
    myConstT = if isConst then constT else id
    matchCase :: Type -> QualID -> (Either VarDecl Expr, Stmt)
    matchCase t p = (cond, body)
      where
        ptrType = derefT $ myConstT t
        cast = dynamicCastE ptrType thisE
        cond = Left $ varDeclInit tempPtrName ptrType cast
        body = ReturnS $ Just $ callE (idE p) [derefE $ idE tempPtrName]

-- | Forward declaration of an ADT's classes
adtForwardDeclaration :: ADT -> [Decl]
adtForwardDeclaration adt@(ADT { adtNamespace = ns, adtName = nm, adtConstructors = ctors }) =
  [FwdDecl $ ForwardDecl ClassFD (qname (map name ns) nm)] ++
  [constructorForwardDeclaration adt c | c <- ctors]

-- | Forward declaration of a constructor
constructorForwardDeclaration :: ADT -> Constructor -> Decl
constructorForwardDeclaration (ADT { adtNamespace = ns }) (Constructor { conName = nm }) =
  FwdDecl $ ForwardDecl ClassFD (qname (map name ns) nm)

adtDeclaration :: ADT -> [Decl]
adtDeclaration adt@(ADT {adtNamespace = ns, adtName = adtName
                        , adtPointer = ptr, adtConstructors = ctors}) =
  let
    classBody = VisibilityDecl Public : classMethods
    classMethods = [ adtDestructor adt
                   , matcher True False adt
                   , matcher False False adt
                   ]
    classDecl = CDecl $ ClassDecl (uname adtName) Nothing classBody
  in [classDecl]

-- | Create an empty virtual destructor for the class
adtDestructor (ADT {adtName = name}) =
  FDecl $ destructorFunDef (uname ("~" <> name)) ["virtual"] [] []

constructorDeclaration :: ADT -> Constructor -> [Decl]
constructorDeclaration adt@(ADT {adtRequirements = reqs})
                       con@(Constructor {conName = name,
                                         conFields = fields}) =
  let
    memberDecls = map fieldMember fields
    ctor = constructorCtor con
    accessors = concatMap fieldAccessors fields
    classMethods = [constructorCtor con] ++
                   [copyCtor True con | copyConstructible reqs] ++
                   [copyCtor False con | moveConstructible reqs] ++
                   [operatorAssign True con | copyAssignable reqs] ++
                   [operatorAssign False con | moveAssignable reqs]
    classBody = VisibilityDecl Private : memberDecls ++
                VisibilityDecl Public : classMethods ++ accessors
    classDecl = CDecl $
                ClassDecl (uname name) (Just $ uname $ adtName adt) classBody
    ctorFn = constructorCtorWrapper adt con
  in [classDecl, ctorFn]

constructorCtor con@(Constructor {conName = name, conFields = fields}) =
  let paramNames = map (uname . mangleForParam . fieldName) fields
      paramDecls = [varDecl name (fieldType f)
                   | (name, f) <- zip paramNames fields]
      params = [takeForInit (fieldRequirements f) (idE name)
               | (f, name) <- zip fields paramNames]
      inits = zip (map (mangleForMember . fieldName) fields) params
  in FDecl $ constructorFunDef (uname name) [] paramDecls [] inits []

copyCtor isConst con@(Constructor {conName = name, conFields = fields}) =
  let argVar = idE tempOtherName
      inits = [let name = mangleForMember $ fieldName f
                in (name, myTake (fieldRequirements f) (dotE argVar name))
              | f <- fields]
      argType = if isConst
                then refT $ constT (unameType name)
                else rvalueRefT (unameType name)
      param = varDecl tempOtherName argType
  in FDecl $ constructorFunDef (uname name) [] [param] [] inits []
  where
    myConstT = if isConst then constT else id

    -- If this is a move assignment operator, move the fields
    myTake reqs = if isConst then id else takeForInit reqs

operatorAssign isConst con@(Constructor {conName = name, conFields = fields}) =
  let argVar = idE tempOtherName
      assigns = [let name = mangleForMember $ fieldName f
                  in ExprS $ unameE name `assignE`
                     myTake (fieldRequirements f) (dotE argVar name)
                | f <- fields]
      body = assigns ++ [ReturnS $ Just $ derefE thisE]
      argType = if isConst
                then refT $ constT (unameType name)
                else rvalueRefT (unameType name)
      param = varDecl tempOtherName argType
      rtype = refT $ unameType name
  in FDecl $ funDef (uname "operator=") [] [param] rtype [] body
  where
    myConstT = if isConst then constT else id

    -- If this is a move assignment operator, move the fields
    myTake reqs = if isConst then id else takeForAssign reqs

-- | Makes the constructor wrapper function for a constructor
constructorCtorWrapper adt con@(Constructor {conName = name, conFields = fields}) =
  let params = [varDecl (uname $ mangleForParam $ fieldName f) (fieldType f)
               | f <- fields]
      args = [takeForInit (fieldRequirements f) (unameE $ fieldName f)
             | f <- fields]
      makeExpr = CallE (typeE $ referenceType adt) [NewE $ callE (unameE name) args]
      body = [ReturnS $ Just makeExpr]
  in FDecl $
     funDef (uname $ mangleCtor name) [] params (referenceType adt) [] body

fieldAccessors field
  | moveAssignable reqs && (preferMove reqs || not (copyAssignable reqs)) =
      [reffer field, constReffer field, taker field, putter field]
  | copyAssignable reqs =
      [getter field, setter field]
  | otherwise =
      error "Attempt to use a non-copyable, non-moveable type"
  where
    reqs = fieldRequirements field

-- | Create a getter method.  @T get_foo() const { return foo_; }@
getter (Field name typ) =
  let body = [ReturnS $ Just (unameE $ mangleForMember name)]
  in FDecl $ funDef (uname $ mangleGetter name) [] [] typ ["const"] body

-- | Create a setter method.  @void set_foo(T foo) { foo_ = foo; }@
setter (Field name typ) =
  let body = [ExprS (unameE (mangleForMember name) `assignE`
                     unameE (mangleForParam name))]
      param = varDecl (uname $ mangleForParam name) typ
  in FDecl $ funDef (uname $ mangleSetter name) [] [param] (rtType void) [] body

-- | Create a taker method.  @T take_foo() { return std::move(foo_); }@
taker (Field name typ) =
  let body = [ReturnS $ Just (moveE $ unameE $ mangleForMember name)]
  in FDecl $ funDef (uname $ mangleTaker name) [] [] typ [] body

-- | Create a putter method.  @void put_foo(T foo) { foo_ = std::move(foo); }@
putter (Field name typ) =
  let body = [ExprS (unameE (mangleForMember name) `assignE`
                     moveE (unameE (mangleForParam name)))]
      param = varDecl (uname $ mangleForParam name) typ
  in FDecl $ funDef (uname $ manglePutter name) [] [param] (rtType void) [] body

-- | Create a mutable reference method.  @T &ref_foo() { return foo_; }@
reffer (Field name typ) =
  let body = [ReturnS $ Just (unameE $ mangleForMember name)]
      rtype = refT $ rtType typ
  in FDecl $ funDef (uname $ mangleReffer name) [] [] rtype [] body

-- | Create a const reference method.
--   @const T &ref_foo() const { return foo_; }@
constReffer (Field name typ) =
  let body = [ReturnS $ Just (unameE $ mangleForMember name)]
      rtype = refT $ constT $ rtType typ
  in FDecl $ funDef (uname $ mangleReffer name) [] [] rtype ["const"] body

fieldMember (Field name typ) =
  VDecl $ varDecl (uname $ mangleForMember name) typ

fieldParamDecl (Field name typ) =
  varDecl (uname $ mangleForParam name) typ

-- | Use a value for initialization.  Prefer to move; fall back to copying.
takeForInit :: Requirements -> Expr -> Expr
takeForInit reqs e
  | moveConstructible reqs && (preferMove reqs || not (copyConstructible reqs)) =
      moveE e
  | copyConstructible reqs =
      e
  | otherwise = error "Attempt to use a non-copyable, non-moveable type"

-- | Use a value for assignment.  Prefer to move; fall back to copying.
takeForAssign :: Requirements -> Expr -> Expr
takeForAssign reqs e
  | moveAssignable reqs && (preferMove reqs || not (copyAssignable reqs)) =
      moveE e
  | copyAssignable reqs =
      e
  | otherwise = error "Attempt to use a non-copyable, non-moveable type"
