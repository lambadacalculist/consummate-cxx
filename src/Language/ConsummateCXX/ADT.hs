-- | The API for declaring algebraic data types.

{-# LANGUAGE OverloadedStrings #-}
module Language.ConsummateCXX.ADT
  (
    SmartPointer,
    applySmartPointer,
    uniquePtr,
    sharedPtr,
    Requirements,
    constructorSatisfiableRequirements,
    satisfiableRequirements,
    resourceReq,
    valueReq,
    dataTypeReq,
    immovableReq,
    ReqType(..),
    ADT(..),
    Constructor(..),
    (=::),
    Field(..),
    fieldRequirements,
    adtClassType,
    adtType
  )
where

import Data.List(foldl')
import Data.Text(Text)

import Language.ConsummateCXX.Internal.ReqType
import Language.ConsummateCXX.Internal.Syntax
import Language.ConsummateCXX.Predefined
import Data.Text.Prettyprint.Doc (nest, vcat, hsep, Pretty(..), (<+>), punctuate, align, fillSep)

newtype SmartPointer = SmartPointer (Type -> ReqType)

applySmartPointer :: SmartPointer -> Type -> ReqType
applySmartPointer (SmartPointer f) t = f t

uniquePtr, sharedPtr :: SmartPointer
uniquePtr = SmartPointer std_unique_ptr
sharedPtr = SmartPointer std_shared_ptr

-- | An algebraic data type definition.
--
-- An ADT definition defines a base class
data ADT
  = ADT
      { -- | Namespace where the ADT is defined
        adtNamespace :: [Text],
        -- | ADT name (abstract base class's name)
        adtName :: Text,
        -- | Smart pointer type used to reference ADT instances
        adtPointer :: SmartPointer,
        -- | Operations implemented by the ADT
        adtRequirements :: Requirements,
        -- | Constructors of the ADT
        adtConstructors :: [Constructor]
      }

instance Pretty ADT where
  pretty (ADT ns name _ _ ctors) =
    vcat [ "ADT" <+> pretty name
         , nest 4 $ vcat $ map pretty ctors
         ]

-- | An ADT constructor type definition.
data Constructor
  = Constructor
      { -- | Constructor name (concrete subclass's name)
        conName :: Text,
        conFields :: [Field]
      }

instance Pretty Constructor where
  pretty (Constructor name fields) =
    pretty name <+> align (vcat $ punctuate "," $ map pretty fields)

-- | A convenience notation for 'Constructor'.
(=::) :: Text -> [Field] -> Constructor
infix 1 =::
n =:: fs = Constructor n fs

data Field
  = Field
      { fieldName :: Text,
        fieldType :: ReqType
      }

instance Pretty Field where
  pretty (Field name rt) = pretty name <+> "::" <+> pretty rt

fieldRequirements :: Field -> Requirements
fieldRequirements f = rtRequirements $ fieldType f

-- | The type of the base class that is defined for an ADT
adtClassType :: ADT -> Type
adtClassType (ADT { adtNamespace = ns, adtName = nm }) =
  qnameType (map name ns) nm

adtType :: ADT -> ReqType
adtType adt@(ADT { adtPointer = ptr }) =
  applySmartPointer ptr $ adtClassType adt

-- | The requirements that an ADT could satisfy, given its constructors.
--   The requirements are determined from the fields' requirements.
constructorSatisfiableRequirements :: [Constructor] -> Requirements
constructorSatisfiableRequirements cs =
  satisfiableRequirements [fieldRequirements f | c <- cs, f <- conFields c]

-- | The requirements that an ADT could satisfy, given requirements of its
--   fields.
--
-- Some requirements can't be satisfied because a field doesn't allow it.
-- For instance, if a field can't be copied, then the whole ADT can't be
-- copied.
satisfiableRequirements :: [Requirements] -> Requirements
satisfiableRequirements reqs = foldl' combine empty reqs
  where
    empty = Requirements False False False False True
    combine (Requirements mc1 cc1 ma1 ca1 pm1)
            (Requirements mc2 cc2 ma2 ca2 pm2) =
      Requirements (mc1 && mc2) (cc1 && cc2) (ma1 && ma2) (ca1 && ca2) (pm1 || pm2)

