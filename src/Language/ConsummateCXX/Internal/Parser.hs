{-| Parse a C++ file and find ADT declarations
-}

{-# LANGUAGE OverloadedStrings, LambdaCase #-}
module Language.ConsummateCXX.Internal.Parser
  (
    ParsedChunk(..),
    parseFile,
    debugDumpParseResult,
    cParseTest
  )
where

import Control.Monad
import Data.Char
import Data.Either (partitionEithers)
import Data.Functor (($>))
import Data.List (nub)
import qualified Data.Text as Text
import Data.Text(Text)
import Data.Text.Prettyprint.Doc(Pretty(..))
import Data.Void
import Text.Megaparsec

import Language.ConsummateCXX.ADT
    (constructorSatisfiableRequirements,
      uniquePtr,
      ADT(ADT, adtName),
      Constructor(Constructor),
      Field(Field) )
import Language.ConsummateCXX.Internal.Syntax
import Language.ConsummateCXX.Internal.ReqType
  ( ReqType (..),
    Requirements,
    copyOnlyReq,
    guessRequirements,
    resourceReq,
    valueReq,
  )

type Parser = Parsec Void Text

data ParsedChunk =
    Uninterpreted Text
  | ParsedDeclaration ADT

-- | C++ attributes recognized by the parser
data AttrSpec =
    ValueAS     -- ^ Value type (movable, copyable, prefer copy)
  | MovableAS   -- ^ A movable type
  | CopyableAS  -- ^ A copyable type
    deriving(Eq)

-- | Determine named requirements from a list of attributes.  Produce an
--   error if the given attributes can't be handled.
attrsRequirements :: [AttrSpec] -> Either String Requirements
attrsRequirements attrs
  | nub_attrs == [ValueAS] = Right valueReq
  | nub_attrs == [MovableAS] = Right resourceReq
  | nub_attrs == [CopyableAS] = Right copyOnlyReq
  | null nub_attrs = Left "Attribute required but no attribute was given"
  | otherwise = Left "Unexpected combination of attributes"
  where
    nub_attrs = nub attrs

-- | Utility function for testing
cParseTest :: Pretty a => Parser a -> Text -> IO ()
cParseTest p input = case runParser p "test input" input of
  Left errs -> putStrLn $ errorBundlePretty errs
  Right good -> print $ pretty good

------------------------------------------------------------------------------
-- Non-parser constants

modifierKeywords = ["const"]
typeKeywords = ["char", "short", "int", "long", "float", "double",
                "unsigned", "signed"]

isIdentifierStart, isIdentifierMiddle :: Char -> Bool
isIdentifierStart c = isAlpha c || c == '_'
isIdentifierMiddle c = isAlphaNum c || c == '_'

------------------------------------------------------------------------------
-- Combinators

-- | Parse and ignore one chunk of whitespace or one comment
whitespaceToken :: Parser ()
whitespaceToken =
  skipSome (satisfy isSpace) <|> cComment <|> cxxComment
  where
    cComment :: Parser ()
    cComment =
      label "comment" $
      void $ chunk "/*" >> skipManyTill anySingle (chunk "*/")
    
    cxxComment :: Parser ()
    cxxComment =
      label "comment" $
      void $ chunk "//" >> skipManyTill anySingle (single '\n')

-- | Parse a token followed by whitespace and comments.
tok :: Parser a -> Parser a
tok p = p <* skipMany whitespaceToken

keyword :: Text -> Parser Text
keyword t = tok (chunk t <* notFollowedBy (satisfy isIdentifierMiddle))

angleBrackets :: Parser a -> Parser a
angleBrackets p = do
  tok $ single '<'
  x <- p
  tok $ single '>'
  return x

block :: Parser a -> Parser a
block p = do
  tok $ single '{'
  x <- p
  tok $ single '}'
  return x

parens :: Parser a -> Parser a
parens p = do
  tok $ single '('
  x <- p
  tok $ single ')'
  return x

attrBrackets :: Parser a -> Parser a
attrBrackets p = do
  tok $ chunk "[["
  x <- p
  tok $ chunk "]]"
  return x

-- | Parse a comma-separated list of things
commaList :: Parser a -> Parser [a]
commaList p = try (commaList1 p) <|> return []

-- | Parse a comma-separated list of 1 or more things
commaList1 :: Parser a -> Parser [a]
commaList1 p = do
  x <- p
  xs <- (tok (single ',') *> commaList1 p) <|> return []
  return $ x : xs

------------------------------------------------------------------------------
-- Syntax elements

identifier :: Parser Identifier
identifier = tok $ label "identifier" $ do
  c <- satisfy isIdentifierStart :: Parser Char
  cs <- takeWhileP Nothing isIdentifierMiddle :: Parser Text
  let name = Text.cons c cs

  -- Keywords are not valid identifiers
  guard $ name `notElem` (modifierKeywords ++ typeKeywords)
  return name

modifierKW, typeKW :: Parser Text
modifierKW = msum $ map keyword modifierKeywords
typeKW = msum $ map keyword typeKeywords

qualifiedIdentifier :: Parser QualID
qualifiedIdentifier = go []
  where
    -- Keep processing name specifiers separated by "::" until the end is found
    go reversedNs = do
      id <- identifier
      targ <- optional templateArg
      let ns = UnqualID id targ
      (tok (chunk "::") *> go (ns : reversedNs)) <|> finish ns reversedNs

    finish ns reversedNs =
      return $ QualID (reverse reversedNs) ns

attrSpec :: Parser AttrSpec
attrSpec = label "consummate-c++ attribute" $ choice
  [ keyword "value" $> ValueAS
  , keyword "movable" $> MovableAS
  , keyword "copyable" $> CopyableAS
  ]

templateArg :: Parser [TemplateArg]
templateArg = fmap (map TypeArg) $ angleBrackets $ commaList typeP

-- | Parse a sequence of attribute specifiers
attributeSpecifiers :: Parser [AttrSpec]
attributeSpecifiers = concat <$> many attributeSpecifierList
  where
    attributeSpecifierList = attrBrackets $ commaList attrSpec

-- | Parse a type
typeP :: Parser Type
typeP =
  try (buildType <$> declspecOfNamedType <*> anonymousDeclarator) <|>
  buildType <$> declspecOfPrimitiveType <*> anonymousDeclarator
  where
    buildType :: Type -> (Type -> Type) -> Type
    buildType t f = f t

-- | Common part of declarator parsing.
declaratorCommon :: Parser (Type -> Type, Maybe Identifier)
declaratorCommon = prefix False
  where
    -- Prefix precedence, e.g. "*foo"
    prefix nonempty = deref <|> suffix nonempty

    deref = do
      single '*'
      (t, i) <- prefix True
      return (t . UnaryType DerefUTO, i)

    -- Suffix precedence, e.g. "foo[]"
    suffix True = parens (prefix True) <|> nameDeclarator
    suffix False = parens (prefix True) <|> nameDeclarator <|> emptyDeclarator

    nameDeclarator = do
      i <- identifier
      return (id, Just i)
    
    emptyDeclarator = do
      -- These characters indicate that there are no more declarator-like parts
      -- following the declarator
      lookAhead $ satisfy (\c -> c == ',' || c == ')' || c == '>' || c == ';')
      return (id, Nothing)

declarator :: Parser (Type -> Type, Identifier)
declarator = do
  (t, m_i) <- declaratorCommon
  case m_i of
    Just i -> return (t, i)
    Nothing -> fail "Declaration does not specify a variable name"

anonymousDeclarator :: Parser (Type -> Type)
anonymousDeclarator = do
  (t, m_i) <- declaratorCommon
  case m_i of
    Nothing -> return t
    Just _ -> fail "Unexpected name in declaration"

-- | Parse a declaration specifier involving a primitive type.
declspecOfPrimitiveType :: Parser Type
declspecOfPrimitiveType = do
  -- Any sequence of modifiers and type keywords 
  items <- many (eitherP modifierKW typeKW)
  let (modifiers, types) = partitionEithers items
  let typeText = Text.unwords types
  when (not $ null modifiers) $
    error "Not implemented: parsing modifiers"
  return $ idT $ uname typeText

-- | Parse a declaration specifier involving a user-defined type.
declspecOfNamedType :: Parser Type
declspecOfNamedType = do
  -- Any sequence of modifiers and a single unqualified ID
  modifiers1 <- many modifierKW
  id <- qualifiedIdentifier
  modifiers2 <- many modifierKW
  let modifiers = modifiers1 ++ modifiers2
  when (not $ null modifiers) $
    error "Not implemented: parsing modifiers"
  return $ idT id

-- | Parse a variable declaration that includes a name
namedDecl :: Parser (Identifier, ReqType)
namedDecl = do
  attrs <- attributeSpecifiers

  -- Due to syntax ambiguity, we need to try both parses
  try (buildDeclWith attrs declspecOfNamedType) <|>
    (buildDeclWith attrs declspecOfPrimitiveType)
  where
    buildDeclWith attrs parseDeclSpec = do
      ds <- parseDeclSpec
      dcl <- declarator
      either fail return $ buildDecl attrs ds dcl
    buildDecl :: [AttrSpec]
              -> Type
              -> (Type -> Type, Identifier)
              -> Either String (Identifier, ReqType)
    buildDecl attrs t (f, id) = do
      let typeOfDecl = f t
      reqs <- if null attrs
              then pure $ guessRequirements typeOfDecl
              else attrsRequirements attrs
      pure (id, ReqType reqs typeOfDecl)

parsedChunk :: Parser ParsedChunk
parsedChunk = ParsedDeclaration <$> adt <|> Uninterpreted <$> notAdt
  where
    -- Read longest possible string until beginning of next adt
    notAdt :: Parser Text
    notAdt =
      Text.pack <$> someTill anySingle (lookAhead (void (chunk "adt class") <|> eof))

field :: Parser Field
field = do
  (name, typ) <- namedDecl
  return $ Field name typ

constructor :: Parser Constructor
constructor = do
  name <- identifier
  fields <- parens $ commaList field
  tok (single ';')
  return $ Constructor name fields

adt :: Parser ADT
adt = do
  tok $ chunk "adt class"
  i <- identifier
  ctors <- block $ some constructor
  single ';'  -- Do not read whitespace after the final semicolon
  let namedRequirements = constructorSatisfiableRequirements ctors
  return $ ADT [] i uniquePtr namedRequirements ctors

file :: Parser [ParsedChunk]
file = many parsedChunk <* eof

parseFile :: FilePath -> Text -> Either String [ParsedChunk]
parseFile path contents = case runParser file path contents of
  Left errs -> Left $ errorBundlePretty errs
  Right x -> Right x

debugDumpParseResult :: [ParsedChunk] -> IO ()
debugDumpParseResult chunks = forM_ chunks $ \case
  Uninterpreted text -> do
    print "Uninterpreted"
    print text
  ParsedDeclaration adt -> do
    print "ADT"
    print $ adtName adt


