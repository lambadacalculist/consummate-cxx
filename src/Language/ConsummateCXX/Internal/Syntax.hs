{-# LANGUAGE OverloadedStrings #-}

module Language.ConsummateCXX.Internal.Syntax where

import qualified Data.Text as Text
import Data.Text (Text)

-- * IDs (names of things)

type Identifier = Text

-- | A qualified identifier, which is a reference to a named entity
--   such as a type or a variable.
--
--   Examples: @std::foo@, @int@, @std::hash<int>::type@
data QualID = QualID {nameSpecs :: ![NameSpec], unqualID :: !UnqualID}

-- | A nested name specifier, which is anything that can be
--   used before @::@ to specify a namespace
type NameSpec = UnqualID

-- | An unqualified identifier, which is a reference to a named entity
--   such as a type or a variable, not including the namespace part.
--   We also use unqualified identifiers for keywords such as @char@.
data UnqualID = UnqualID !Identifier !(Maybe [TemplateArg])

data TemplateArg = TypeArg Type | ValueArg Expr

-- ** Constructing IDs

name :: Identifier -> UnqualID
name n = UnqualID n Nothing

inst :: Identifier -> [TemplateArg] -> UnqualID
inst n args = UnqualID n (Just args)

unqual :: UnqualID -> QualID
unqual i = QualID [] i

uname :: Identifier -> QualID
uname n = unqual (name n)

uinst :: Identifier -> [TemplateArg] -> QualID
uinst n args = unqual (inst n args)

qname :: [NameSpec] -> Identifier -> QualID
qname nss n = QualID nss (name n)

qinst :: [NameSpec] -> Identifier -> [TemplateArg] -> QualID
qinst nss n args = QualID nss (inst n args)

-- * Types

data UnaryTypeOp
  = -- | @T &@
    RefUTO
  | -- | @T &&@
    RValueRefUTO
    -- | @T *@
  | DerefUTO

data Type
  = -- | A named type or template instantiation
    IDType QualID
  | -- | A built-in unary operator, e.g. @&&@
    UnaryType UnaryTypeOp Type
  | QualType [TyQual] Type
    -- | Type of an expression, @decltype(E)@
  | DeclType Expr

class ToType a where
  toType :: a -> Type

instance ToType Type where
  toType t = t

addQualifier :: TyQual -> Type -> Type
addQualifier q (QualType qs t) =
  let qs' = if q `elem` qs then qs else q : qs
   in QualType qs' t
addQualifier q t = QualType [q] t

data TyQual = Const deriving (Eq)

unameType :: Identifier -> Type
unameType = IDType . uname

qnameType :: [NameSpec] -> Identifier -> Type
qnameType ns n = IDType (qname ns n)

data Decl
  = FwdDecl ForwardDecl
  | CDecl ClassDecl
  | FDecl FunDecl
  | VDecl VarDecl
  | VisibilityDecl Visibility
  | TDecl TemplateDecl

-- | A template declaration, @template<typename A, int n> ...@.
data TemplateDecl = TemplateDecl [VarDecl] Decl

data Visibility = Public | Private

data ForwardDeclKind = ClassFD

data ForwardDecl = ForwardDecl ForwardDeclKind QualID

data ClassDecl
  = ClassDecl
      { cdName :: QualID,
        cdBase :: Maybe QualID,
        cdBody :: [Decl]
      }

data FunDecl
  = FunDecl
      { fdName :: QualID,
        -- | Specifiers before function, e.g. \"virtual\"
        fdSpecifiers :: [Text],
        fdParams :: [VarDecl],
        -- | Return type; Nothing for constructors and destructors
        fdReturn :: Maybe Type,
        -- | Qualifiers after function, e.g. \"const\", which apply to
        --   the \"this\" instance of a method
        fdThisQualifiers :: [Text],
        -- | Initializers in a class constructor, e.g. @a(0)@ in
        --   @Foo() : a(0) {}@.
        fdInitializers :: [(Identifier, Expr)],
        fdBody :: Maybe [Stmt]
      }

funDef :: ToType t =>
          QualID -> [Text] -> [VarDecl] -> t -> [Text] -> [Stmt] -> FunDecl
funDef name specs params rtype thisQualifiers body =
  FunDecl name specs params (Just $ toType rtype) thisQualifiers [] (Just body)

constructorFunDef ::
  QualID ->
  [Text] ->
  [VarDecl] ->
  [Text] ->
  [(Identifier, Expr)] ->
  [Stmt] ->
  FunDecl
constructorFunDef name specs params thisQualifiers inits body =
  FunDecl name specs params Nothing thisQualifiers inits (Just body)

destructorFunDef ::
  QualID ->
  [Text] ->
  [Text] ->
  [Stmt] ->
  FunDecl
destructorFunDef name specs thisQualifiers body =
  FunDecl name specs [] Nothing thisQualifiers [] (Just body)

constQualifier :: Text
constQualifier = "const"

data VarDecl
  = VarDecl
      { vdName :: !QualID,
        vdType :: !Type,
        -- | Initial value for declaration; only allowed in some contexts
        vdInit :: !(Maybe Expr)
      }

-- | A type variable declaration, @typename T@.
typeVarDecl :: QualID -> VarDecl
typeVarDecl qid = VarDecl qid (unameType "typename") Nothing

-- | A variable declaration with no initializer
varDecl :: ToType a => QualID -> a -> VarDecl
varDecl id t = VarDecl id (toType t) Nothing

-- | A variable declaration with no initializer
varDeclInit :: ToType a => QualID -> a -> Expr -> VarDecl
varDeclInit id t e = VarDecl id (toType t) (Just e)

data Stmt
  = DeclS Decl
  | ExprS Expr
  | ReturnS (Maybe Expr)
  | ThrowS Expr
  | IfS (Either VarDecl Expr) Stmt (Maybe Stmt)
  | BlockS [Stmt]

data Expr
  = IDE QualID
  | TypeE Type
  | StringE Text
  | DotE Expr !Text
  | BinE !Prec !Assoc !Text Expr Expr -- ^ Infix binary expression
  | UnaryE !Text Expr                 -- ^ Prefix unary expression
  | CallE Expr [Expr]           -- ^ Function call
  | NewE Expr
  | LambdaE LambdaCapture [VarDecl] Type [Stmt]

data LambdaCapture =
  DefaultReferenceLC            -- ^ Default capture by reference @[&]@

type Prec = Int
data Assoc = AssocLeft | AssocRight | AssocNone deriving(Eq)

-- | A C++ file
data Module = Module ![Decl]

-------------------------------------------------------------------------------
-- Types

idT :: QualID -> Type
idT = IDType

constT :: Type -> Type
constT = QualType [Const]

derefT :: Type -> Type
derefT = UnaryType DerefUTO

refT :: Type -> Type
refT = UnaryType RefUTO

rvalueRefT :: Type -> Type
rvalueRefT = UnaryType RValueRefUTO

-------------------------------------------------------------------------------
-- Expressions

unameE :: Text -> Expr
unameE t = idE $ uname t

idE :: QualID -> Expr
idE t = IDE t

thisE :: Expr
thisE = unameE "this"

typeE :: Type -> Expr
typeE t = TypeE t

stringE :: Text -> Expr
stringE t = StringE t

callE :: Expr -> [Expr] -> Expr
callE f xs = CallE f xs

dotE :: Expr -> Text -> Expr
dotE e t = DotE e t

assignE :: Expr -> Expr -> Expr
assignE lhs rhs = BinE 16 AssocRight "=" lhs rhs

derefE :: Expr -> Expr
derefE e = UnaryE "*" e

moveE :: Expr -> Expr
moveE e = callE (idE $ qname [name "std"] "move") [e]

declvalE :: Type -> Expr
declvalE t = callE (idE $ qinst [name "std"] "declval" [TypeArg t]) []

dynamicCastE :: Type -> Expr -> Expr
dynamicCastE t e = callE (idE $ uinst "dynamic_cast" [TypeArg t]) [e]

-------------------------------------------------------------------------------
-- Statements

ifElseS :: Stmt -> [(Either VarDecl Expr, Stmt)] -> Stmt
ifElseS fallback ((cond, body):conds) =
  IfS cond body (Just $ ifElseS fallback conds)
ifElseS s [] = s

