{-# LANGUAGE OverloadedStrings #-}
module Language.ConsummateCXX.Internal.Render where

import Data.Maybe(maybeToList)
import Data.Text(Text)
import qualified Data.Text as Text
import Debug.Trace

import Language.ConsummateCXX.Internal.Layout
import Language.ConsummateCXX.Internal.Syntax

-- | Format items as a parameter list that can be split across lines.
--
-- When put on one line, a list looks like @(a, b)@ or @<a, b>@.
-- When split across lines, each item begins on its own indented line:
--
-- > (
-- >     a,
-- >     b
-- > )
renderParameterList :: Bool -> [Render] -> Render
renderParameterList angles xs
  | null xs   = inline -- Do not split empty list across lines
  | otherwise = singleOrMultiLine inline multiline
  where
    leftDelim = if angles then token "<" else token "("
    rightDelim = if angles then token ">" else token ")"
    punctuated = punctuate (token ",") xs
    inline = leftDelim <> (suppressBreaks $ hsep punctuated) <> rightDelim
    multiline = leftDelim <>
                nest 4 (newline <> vcat punctuated) <>
                newline <> rightDelim

renderFunParameterList = renderParameterList False
renderTmpParameterList = renderParameterList True

emptyBlock = token "{}"

-- | Render a block of statements or declarations in braces.
renderBlock :: [Render] -> Render
renderBlock [] = emptyBlock
renderBlock xs = token "{" <>
                 (nest 4 $ newline <> vcat (map prune xs)) <>
                 newline <> token "}"

appendSemi :: Render -> Render
appendSemi r = r <> token ";"

appendSemiIf :: Bool -> Render -> Render
appendSemiIf True r = appendSemi r
appendSemiIf False r = r

renderUnqualID :: UnqualID -> Render
renderUnqualID (UnqualID ident args) =
  let render_id = token ident
      render_args = fmap (renderTmpParameterList . map renderTemplateArg) args
  in prefix render_id render_args

renderTemplateArg (TypeArg t) = renderType t
renderTemplateArg (ValueArg e) = renderExpr e

renderID :: QualID -> Render
renderID (QualID specs id) = foldr applySpec (renderUnqualID id) specs
  where
    applySpec spec r = renderUnqualID spec <> token "::" <> r

renderExpr e = renderExprPrec 18 e

renderExprPrec p e = case e of
  IDE qid -> renderID qid
  TypeE t -> renderType t
  StringE t -> token t
  DotE e t -> renderExprPrec 0 e <> token ("." <> t)
  BinE q assoc op l r ->
    let lp = if assoc == AssocLeft then q + 1 else q
        rp = if assoc == AssocRight then q + 1 else q
        doc = sep [hsep [renderExprPrec lp l, token op],
                   renderExprPrec rp r]
    in if q >= p then parens doc else doc
  UnaryE op e -> token op <> renderExpr e
  CallE f xs ->
    renderExprPrec 2 f <> renderFunParameterList (map renderExpr xs)
  NewE e -> hsep [token "new", renderExprPrec 3 e]

-- | Shows a type, maybe as part of a declarator.
renderType t = renderSpecAndDecl t Nothing

renderQual :: TyQual -> Render
renderQual Const = token "const"

applyTypeOp RefUTO x = prefix (token "&") x
applyTypeOp RValueRefUTO x = prefix (token "&&") x
applyTypeOp DerefUTO x = prefix (token "*") x

prefix x Nothing = x
prefix x (Just y) = x <> y

suffix Nothing x = x
suffix (Just y) x = y <> x

-- | Render a forward declaration such as "class Foo".
--   Does not include a semicolon.
renderForwardDecl :: ForwardDecl -> Render
renderForwardDecl (ForwardDecl ClassFD id) =
  hsep [token "class", renderID id]

renderClassDecl :: ClassDecl -> Render
renderClassDecl (ClassDecl name base body) = let
  r_base = fmap (\b -> sep [token ": public", renderID b]) base
  r_body = renderBlock $ map renderDeclSemi body
  in hsep ([token "class", renderID name] ++ maybeToList r_base) `stack`
     r_body <> token ";"

renderVisibility :: Visibility -> Render
renderVisibility Public = token "public:"
renderVisibility Private = token "private:"

-- | Render the common part of a declaration: a specifier and declarator.
--   For example "int x" or void (*f(int x))()".
renderSpecAndDecl :: Type -- ^ Type to render as specifier and declarator
                  -> Maybe Render -- ^ Declared thing in declarator
                  -> Render       -- ^ Rendered declaration
renderSpecAndDecl (IDType qid) Nothing =
  renderID qid 

renderSpecAndDecl (IDType qid) (Just decl) =
  sep [renderID qid, decl]

renderSpecAndDecl (QualType quals t) m_decl =
  hsep (map renderQual quals ++ [renderSpecAndDecl t m_decl])

renderSpecAndDecl (UnaryType op t) m_decl =
  renderSpecAndDecl t (Just $ applyTypeOp op m_decl)

renderSpecAndDecl (DeclType e) m_decl = case m_decl of
  Nothing -> r_type
  Just d -> singleOrMultiLine (single d) (multi d)
  where
    r_type = token "decltype" <> parens (renderExpr e)
    single d = hsep [suppressBreaks r_type, d]
    multi d = r_type `stack` d

-- | Render a variable declaration or definition.
--   Does not append a semicolon.
renderVarDecl :: VarDecl -> Render
renderVarDecl (VarDecl {vdName = n, vdType = t, vdInit = m_init}) = let
  d = renderSpecAndDecl t (Just $ renderID n)
  in case m_init of
       Nothing -> d
       Just e -> hang (hsep [d, token "="]) 4 (renderExpr e)

-- | Render a function declaration or definition.
--   Does not append a semicolon.
renderFunDecl :: Bool -- ^ Whether to add a semicolon.  A semicolon is only
                      --   added for declarations (function without body).
              -> FunDecl
              -> Render
renderFunDecl needSemicolon fd = let
  specifierList = map token $ fdSpecifiers fd
  thisQualifierList = map token $ fdThisQualifiers fd
  parameterList = renderFunParameterList $ map renderVarDecl $ fdParams fd
  initializerList =
    punctuate (token ",") $ map renderInitializer $ fdInitializers fd

  -- The part of the declaration that uses function call syntax
  callPart = renderID (fdName fd) <> parameterList

  declaration = case fdReturn fd of
    Just returnType -> renderSpecAndDecl returnType (Just callPart)
    Nothing -> callPart

  -- Declaration including qualifiers at the beginning and end
  -- like "virtual F() const"
  declarationWithQuals =
    hsep (specifierList ++ declaration : thisQualifierList) 
  
  appendInitializers t =
    if null $ fdInitializers fd
    then t
    else hang (sep [t, token ":"]) 4 (sep initializerList)

  appendBody t = case fdBody fd of
    Nothing -> appendSemiIf needSemicolon t
    Just [] -> sep [t, emptyBlock] -- Empty block can go on the same line
    Just b -> t `stack` renderStmt (BlockS b)
  in (appendBody . appendInitializers) declarationWithQuals
  where
    renderInitializer (id, e) = token id <> parens (renderExpr e)

-- | Render a declaration
renderDeclGeneral :: Bool -> Decl -> Render
renderDeclGeneral needSemi (FwdDecl fd) =
  appendSemiIf needSemi $ renderForwardDecl fd

renderDeclGeneral needSemi (FDecl fd) =
  renderFunDecl needSemi fd

renderDeclGeneral needSemi (CDecl cd) =
  renderClassDecl cd

renderDeclGeneral needSemi (VDecl vd) =
  appendSemiIf needSemi $ renderVarDecl vd

renderDeclGeneral needSemi (VisibilityDecl v) =
  renderVisibility v

renderDeclGeneral needSemi (TDecl td) =
  renderTemplateDecl needSemi td

renderTemplateDecl needSemi (TemplateDecl params d) = let
  r_tmpl = token "template" <> renderTmpParameterList (map renderVarDecl params)
  in sep [r_tmpl, renderDeclGeneral needSemi d]

renderDeclSemi d = renderDeclGeneral True d
renderDecl d = renderDeclGeneral False d

renderStmt :: Stmt -> Render
renderStmt s = case s of
  DeclS d -> renderDeclSemi d
  ExprS e -> appendSemi $ renderExpr e
  ReturnS Nothing -> token "return;"
  ReturnS (Just e) -> appendSemi $ hsep [token "return", renderExpr e]
  ThrowS e -> appendSemi $ hsep [token "throw", renderExpr e]
  IfS cond t e -> let
    r_if = sep [token "if", parens (either renderVarDecl renderExpr cond)]
    r_t = renderStmt t
    r_e = fmap (\s -> sep [token "else", renderStmt s]) e
    in vcat ([r_if, r_t] ++ maybeToList r_e)
  BlockS ss -> renderBlock $ map renderStmt ss

renderModule :: Module -> Render
renderModule (Module ds) = vcat $ map renderDeclSemi ds

complexTestBody =
  IfS
  (Left $ varDecl (uname "p") (refT (idT $ uname "int")))
  (ReturnS $ Just $ callE (idE $ uname "f_X") [])
  (Just $ ThrowS $ callE (idE $ uname "bad_cast") [])

complexTestFun =
  funDef (uname "foo") [] [] (idT $ uname "void") [] [complexTestBody]

testc = testRender $ renderFunDecl True complexTestFun

