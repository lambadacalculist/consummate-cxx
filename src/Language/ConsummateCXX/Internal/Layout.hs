{-# LANGUAGE BangPatterns, OverloadedStrings #-}
module Language.ConsummateCXX.Internal.Layout where

import Control.Applicative
import Control.Monad
import Debug.Trace

import Data.Function(on)
import qualified Data.List as List
import qualified Data.Text as Text
import Data.Text(Text)

import Language.ConsummateCXX.Internal.Layout.Badness
import Language.ConsummateCXX.Internal.Layout.Fragment
import qualified Language.ConsummateCXX.Internal.Layout.ParetoSet as ParetoSet

-- | Get the end position of a fragment
fragmentPos :: Fragment -> Int
fragmentPos f = b_end $ fl_metric f

-- | A rendered fragment.  The rendered fragment has additional data
--   that is used for rendering decisions.
data RFragment = RFragment
  { rf_fragment :: !Fragment -- ^ Partially rendered lines, may be aborted
  , rf_badness :: Int        -- ^ Badness of the fragment
  }

rFragment :: BadnessOptions -> Fragment -> RFragment
rFragment opts fragment =
  RFragment fragment (fragmentBadness opts fragment)

appendRFragment :: BadnessOptions -> RFragment -> Fragment -> RFragment
appendRFragment opts rf f =
  rFragment opts (catFragments opts (rf_fragment rf) f)

-- | A local context in which rendering is done.
data RContext = RContext
  { rc_options :: !BadnessOptions
  , rc_indent :: !Int -- ^ Indentation of first character after line break
  , rc_column :: !Int   -- ^ Column of next character on current line
  , rc_allowBreak :: !Bool -- ^ Whether optional line breaks are allowed
  }

-- | Set the context's position to the end of the fragment.
positionContextAtFragment :: RContext -> RFragment -> RContext
positionContextAtFragment c f =
  c {rc_column = fragmentPos $ rf_fragment f}

-- | A text renderer.  Produces alternative text formattings with
--   information about badness and positioning, used for combining
--   with other text and selecting the best rendering.
newtype Render =
  Render {runRender :: RContext -> ParetoSet.ParetoSet RFragment}

-- | Rendering forms a semigroup under concatenation
instance Semigroup Render where
  (<>) = abut

-- | Rendering forms a monoid under concatenation
instance Monoid Render where
  mempty = emptyRender

fragmentsToSet :: [RFragment] -> ParetoSet.ParetoSet RFragment
fragmentsToSet fs = List.foldl' insertItem ParetoSet.empty fs
  where
    insertItem s rf = ParetoSet.insert (fl_metric $ rf_fragment rf) rf s

render :: Render -> Text
render r = renderOpts defaultBadnessOptions r

initialRenderContext :: BadnessOptions -> RContext
initialRenderContext opts =
  RContext
  { rc_options = opts
  , rc_indent = 0
  , rc_column = 0
  , rc_allowBreak = True
  }

renderOpts :: BadnessOptions -> Render -> Text
renderOpts opts r =
  case ParetoSet.values $ runRender (prune r) (initialRenderContext opts) of
    [x] -> fragmentToText (rf_fragment x)
    [] -> error "No rendering found"
    _ -> error "Rendering failed"

-- | Run rendering and return alternative renders with their badness
renderDebug :: BadnessOptions -> Render -> [(Int, Text)]
renderDebug opts r = let
  fragments = ParetoSet.values $ runRender r (initialRenderContext opts)
  in [(fragmentBadness opts f, fragmentToText f)
     | rf <- fragments, let f = rf_fragment rf]

-- | Utility function to make a fragment using information from the context
makeCFragment :: RContext -> Fragment -> ParetoSet.ParetoSet RFragment
makeCFragment c f =
  let rf = rFragment (rc_options c) f
  in ParetoSet.singleton (fl_metric $ rf_fragment rf) rf

emptyRender :: Render
emptyRender = Render $ \c -> makeCFragment c (emptyFragment (rc_column c))

token :: Text -> Render
token t = Render $ \c -> makeCFragment c (textFragment (rc_options c) (rc_column c) t)

-- | Start a new line
newline :: Render
newline = Render $ \c -> makeCFragment c (lineBreakFragment (rc_options c) (rc_column c) (rc_indent c))

-- | Suppress optional line breaking within the render.
--   This is useful with alternatives to require singe-line rendering in
--   one alternative layout.
suppressBreaks :: Render -> Render
suppressBreaks r = Render $ \c -> runRender r $ c {rc_allowBreak = False}

-- | Make several alternative renders
alternatives :: [Render] -> Render
alternatives rs = Render $ \c -> debugConcat $ map (flip runRender c) rs
  where
    debugConcat :: [ParetoSet.ParetoSet RFragment] -> ParetoSet.ParetoSet RFragment
    debugConcat xs
      | count (not . ParetoSet.null) xs <= 1 =
          -- No growth; no debugging message needed
          ParetoSet.unions xs
      | otherwise = let
          showSample f = fragmentToText (rf_fragment f)
          samples = Text.intercalate "\n" $ concatMap (map showSample . take 3 . ParetoSet.values) xs
          msg = "Concatenating\n" ++ Text.unpack samples
          in ParetoSet.unions xs

    count f xs = length $ filter f xs

-- | Do a rendering only when discretionary breaks are allowed.
--   This should be used within 'alternatives' so that there is a fallback
--   when breaks are disallowed.
whenAllowBreak :: Render -> Render
whenAllowBreak r =
  Render $ \c -> if rc_allowBreak c then runRender r c else ParetoSet.empty

-- | Render in one of two ways, where the second way is a multi-line
--   rendering.  When line breaks are suppressed, the multi-line rendering
--   is skipped.
singleOrMultiLine :: Render -- ^ Single line rendering
                  -> Render -- ^ Multi line rendering
                  -> Render -- ^ Rendering with both alternatives
singleOrMultiLine s m = alternatives [prune $ suppressBreaks s, whenAllowBreak m]

-- | Increase the indentation level of newlines in the document
nest :: Int -> Render -> Render
nest n r = Render $ \c -> runRender r $ c {rc_indent = rc_indent c + n}

-- | Increases the indentation level to the current position
align :: Render -> Render
align r = Render $ \c -> runRender r (c {rc_indent = rc_column c})

-- | Concatenate two renders
abut :: Render -> Render -> Render
abut r1 r2 = Render $ \c -> fragmentsToSet $ do
  frag1 <- ParetoSet.values $ runRender r1 c
  frag2 <- ParetoSet.values $ runRender r2 (positionContextAtFragment c frag1)
  return $ appendRFragment (rc_options c) frag1 (rf_fragment frag2)

-- | Select one layout and discard alternatives using the given function.
--   The function is only called if at least one rendering was produced.
pruneBy :: (RContext -> [RFragment] -> RFragment) -> Render -> Render
pruneBy f r = Render $ \c -> let
  fs = ParetoSet.values $ runRender r c
  in if null fs
     then ParetoSet.empty -- No rendering was produced
     else let r = f c fs
          in ParetoSet.singleton (fl_metric $ rf_fragment r) r

-- | Take the minimum as in List.minimumBy, but stop immediately if
--   an item with value 0 is found.
minByBounded :: (a -> Int) -> [a] -> a
minByBounded f (x:xs) = go x (f x) xs
  where
    go bestValue 0 _ = bestValue -- Early cutoff
    go bestValue (!bestCost) (x:xs)
      | xCost < bestCost = go x xCost xs
      | otherwise        = go bestValue bestCost xs
      where
        xCost = f x

    go bestValue _ [] = bestValue

-- | Select the locally optimal layout and discard alternatives
prune :: Render -> Render
prune = pruneBy leastBadness
  where
    leastBadness _ fs =
      minByBounded rf_badness fs

{-
-- | Select the optimal layout assuming that all characters in the first
--   and last lines are overfill.
--
--   This function is intended for pruning single-line layouts, as it will
--   select the shortest layout in that case.
pruneShortest :: Render -> Render
pruneShortest = pruneBy shortest
  where
    shortest c fs = head fs -- minByBounded (computeBadness c) fs
    computeBadness c f = fragmentOverfillingBadness (rc_options c) (rf_fragment f)

-- | Select the optimal layout and discard alternatives, assuming
--   that the given render is a complete line
pruneLine :: Render -> Render
pruneLine = pruneBy leastBadness
  where
    leastBadness c fs = head fs -- minByBounded (computeBadness c) fs

    computeBadness c f = fragmentCompleteBadness (rc_options c) (p_firstColumn $ rf_startPos f) (rf_fragment f)

    debug_info fs = let
      texts = [Text.unpack $ fragmentToText (rf_fragment $ head fs) | f <- take 3 fs]
      in List.intercalate "\n" (show (length fs) : texts)
-}

-- | Soft line break with space.  Either render the next item with a space
--   or render it on a new indented line.
softBreakSpace :: Int -> Render -> Render
softBreakSpace indent r =
  singleOrMultiLine (token " " <> r) (nest indent $ newline <> r)

hang :: Render -> Int -> Render -> Render
hang r1 n r2 = r1 <> softBreakSpace n r2

cat :: [Render] -> Render
cat = mconcat

punctuate :: Render -> [Render] -> [Render]
punctuate _ [] = []
punctuate _ [x] = [x]
punctuate separator (x:xs) = abut x separator : punctuate separator xs 

hsep :: [Render] -> Render
hsep xs = cat $ punctuate (token " ") xs

stack r1 r2 = r1 <> newline <> r2

vcat :: [Render] -> Render
vcat [] = emptyRender
vcat xs = foldr1 stack xs

-- | Render either on one line or with line breaks.
--   If on one line, internal breaks are suppressed.
sep :: [Render] -> Render
sep xs = singleOrMultiLine (hsep xs) (vcat xs)

-- | Render separated by either spaces or line breaks.
--   Internal line breaks are allowed.
xsep :: [Render] -> Render
xsep xs = alternatives [hsep xs, vcat xs]

parens :: Render -> Render
parens x = cat [token "(", x, token ")"]

angles :: Render -> Render
angles x = cat [token "<", x, token ">"]

-- | Render a list either with spaces or vertically aligned
aligned :: [Render] -> Render
aligned xs = singleOrMultiLine (hsep xs) (align $ vcat xs)

-- | Display rendered text for testing
testRender :: Render -> IO ()
testRender r = putStrLn $ Text.unpack $ render r
