
{-# LANGUAGE OverloadedStrings #-}
module Language.ConsummateCXX.Internal.ReqType where

import Data.Text.Prettyprint.Doc ((<+>), sep, punctuate, Pretty(..))
import Data.Maybe (catMaybes)

import Language.ConsummateCXX.Internal.Syntax
import Language.ConsummateCXX.Internal.Layout(render)
import Language.ConsummateCXX.Internal.Render

-- | Operations that a type may be expected to support, called
--   \"Named Requirements\" in the C++ standard.
--
--   This information is used to decide whether moving or copying
--   is possible, and which one to prefer.
data Requirements =
  Requirements
  { moveConstructible :: !Bool
  , copyConstructible :: !Bool
  , moveAssignable    :: !Bool
  , copyAssignable    :: !Bool
  , preferMove        :: !Bool
  }

-- | A type that can only be moved, such as unique_ptr.  Typically a proxy
--   for some allocated resource.
resourceReq :: Requirements
resourceReq = Requirements True False True False True

-- | A pass-by-value type, such as int.  Prefer to pass by value.
valueReq :: Requirements
valueReq = Requirements True True True True False

-- | A type that can be moved and copied, such as std::string.
--   Prefer to pass by move.
dataTypeReq :: Requirements
dataTypeReq = Requirements True True True True True

-- | A data type that can only be copied.  Typical of types that were
--   defined before C++11.
copyOnlyReq :: Requirements
copyOnlyReq = Requirements False True False True False

immovableReq :: Requirements
immovableReq = Requirements False False False False False

data ReqType = ReqType { rtRequirements :: !Requirements, rtType :: !Type }

instance ToType ReqType where
  toType = rtType

instance Pretty ReqType where
  pretty (ReqType r t) =
    let rDoc = requirementsAsDoc r
        tDoc = pretty $ render $ renderType t
    in maybe tDoc (tDoc <+>) rDoc
    where
      requirementsAsDoc Requirements{moveConstructible=mc, copyConstructible=cc,
                                     moveAssignable=ma, copyAssignable=ca,
                                     preferMove=pm} =
        let m = case (mc, ma) of
                  (True,  True)  -> Just "movable"
                  (True, False)  -> Just "move-constructible"
                  (False, True)  -> Just "move-assignable"
                  (False, False) -> Nothing
            c = case (cc, ca) of
                  (True,  True)  -> Just "copyable"
                  (True, False)  -> Just "copy-constructible"
                  (False, True)  -> Just "copy-assignable"
                  (False, False) -> Nothing
            x = if pm then Nothing else Just "prefer-copy"
        in case catMaybes [m, c, x] of
          [] -> Nothing
          l -> Just $ "[[" <> sep (punctuate "," l) <> "]]"

checkReq :: (Requirements -> Bool) -> ReqType -> Bool
checkReq f t = f (rtRequirements t)

resource :: Type -> ReqType
resource t = ReqType resourceReq t

value :: Type -> ReqType
value t = ReqType valueReq t

dataType :: Type -> ReqType
dataType t = ReqType dataTypeReq t

-- | Guesses a type's named requirements based on heuristics.
--
-- If the type is a @std::unique_ptr<T>@, it's a resource.
-- If the type is a primitive numeric type, it's a value.
-- Otherwise, it is a data type.
guessRequirements :: Type -> Requirements
guessRequirements t
  | isUniquePtr t = resourceReq
  | isNumeric t = valueReq
  | otherwise = dataTypeReq
  where
    -- Match std::unique_ptr<T>
    isUniquePtr (IDType (QualID specs (UnqualID idName (Just [_]))))
      | isStdSpecs specs && idName == "unique_ptr" = True
    isUniquePtr _ = False

    isStdSpecs [UnqualID idName Nothing]
      | idName == "std" = True
    isStdSpecs _ = False

    isNumeric (IDType (QualID [] (UnqualID idName Nothing)))
      | idName == "char" || idName == "short" || idName == "int" ||
        idName == "long" || idName == "float" || idName == "double" = True
    isNumeric _ = False

