
module Language.ConsummateCXX.Internal.Layout.ParetoSet
  (
    ParetoSet,
    empty,
    singleton,
    insert,
    union,
    unions,
    null,
    values
  )
where

import Prelude hiding(null)

import qualified Data.List as List

import Language.ConsummateCXX.Internal.Layout.Fragment(LayoutMetric, (<&), (<=&))

-- | A set of pareto-optimal values.
--
--   Naive implementation, should improve.
newtype ParetoSet a = ParetoSet [Item a]

data Item a = Item {-# UNPACK #-} !LayoutMetric !a
metric (Item m _) = m
value (Item _ v) = v

empty :: ParetoSet a
empty = ParetoSet []

singleton :: LayoutMetric -> a -> ParetoSet a
singleton m x = insert m x empty

-- | Insert an item into the set.  Non-optimal items are
--   removed during insertion.
insert :: LayoutMetric -> a -> ParetoSet a -> ParetoSet a
insert m x (ParetoSet xs)
  | any ((<=& m) . metric) xs =
    -- Inferior or equal to an item that's already in the set
    ParetoSet xs
  | otherwise =
    -- Add to set, filter out inferior items
    ParetoSet $ Item m x : filter (not . (m <&) . metric) xs

union :: ParetoSet a -> ParetoSet a -> ParetoSet a
union (ParetoSet xs) s = List.foldl' insertItem s xs
  where
    insertItem s (Item m x) = insert m x s

unions :: [ParetoSet a] -> ParetoSet a
unions xs = List.foldl' union empty xs

null :: ParetoSet a -> Bool
null (ParetoSet []) = True
null _ = False

values :: ParetoSet a -> [a]
values (ParetoSet items) = map value items
