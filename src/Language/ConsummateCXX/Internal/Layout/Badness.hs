{-|
Layout badness calculations, used for optimizing the positioning of
formatted text.

Text that fits in a single line has zero badness.  This allows
for some early cutoff in the layout algorithm.
-}
{-# LANGUAGE BangPatterns, OverloadedStrings #-}
module Language.ConsummateCXX.Internal.Layout.Badness where

-- | Options that influence badness calculation
data BadnessOptions = BadnessOptions
  { -- | Penalty per character for running off the end of the line
    bs_overfillPenalty :: !Int
    -- | Penalty for having a line (favor fewer lines)
  , bs_intrinsicLinePenalty :: !Int
    -- | Text width that incurs no overfill penalty
  , bs_ribbonWidth :: !Int
  }

defaultBadnessOptions :: BadnessOptions
defaultBadnessOptions = BadnessOptions
  { bs_overfillPenalty = 10
  , bs_intrinsicLinePenalty = 2
  , bs_ribbonWidth = 80
  }

-- | Compute overfill penalty for the given amount of overfill.
explicitOverfill :: BadnessOptions
                 -> Int -- ^ Number of overfill characters
                 -> Int -- ^ Overfill penalty
explicitOverfill options n = bs_overfillPenalty options * n

-- | Calculate the badness of starting a new line
newlineBadness :: BadnessOptions -> Int
newlineBadness options = bs_intrinsicLinePenalty options

-- | Compute the number of overfill characters in a text string.
countOverfill :: BadnessOptions
              -> Int -- ^ Position of first character 
              -> Int -- ^ Number of characters in string
              -> Int -- ^ Number of overfill characters
countOverfill opts first n = let
  -- Number of characters that can fit without overfill
  goodWidth = max 0 $ bs_ribbonWidth opts - first
  in max 0 $ n - goodWidth

