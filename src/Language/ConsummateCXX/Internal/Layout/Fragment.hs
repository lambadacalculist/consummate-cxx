{-| Rendered text fragments.
-}
{-# LANGUAGE OverloadedStrings #-}
module Language.ConsummateCXX.Internal.Layout.Fragment where

import Data.Function(on)
import qualified Data.Text as Text
import Data.Text(Text)

import Language.ConsummateCXX.Internal.Layout.Badness

-- | A metric for how good a fragment's text layout is.
--   Smaller values are better.
data LayoutMetric = LayoutMetric
  { b_overfill :: !Int -- ^ Number of overfill characters
  , b_height :: !Int   -- ^ Number of complete lines
  , b_end :: !Int      -- ^ Position after end of last line
  } deriving(Eq, Show)

-- | Pareto-optimal comparsion.  @x <=& y@ is True if x equal or better than
--   y in all metrics.
(<=&) :: LayoutMetric -> LayoutMetric -> Bool
x <=& y = ((<=) `on` b_overfill) x y &&
          ((<=) `on` b_height) x y &&
          ((<=) `on` b_end) x y

-- | Pareto-optimal comparsion.  @x <& y@ is True if x is strictly better
--   than y.
(<&) :: LayoutMetric -> LayoutMetric -> Bool
x <& y = x <=& y && not (x == y)

addMetrics :: LayoutMetric -> LayoutMetric -> LayoutMetric
addMetrics (LayoutMetric o1 h1 _) (LayoutMetric o2 h2 e) =
  LayoutMetric (o1 + o2) (h1 + h2) e

-- | A rendered text fragment with incomplete first and last lines, which
--   can be concatenated with other fragments.
--
--   Multi-line fragments have partial cost information that is used to
--   speed up rendering.
data Fragment =
    -- | A fragment with no internal line breaks
    FragmentSpan
    { fl_solo :: !Text -- ^ Partial line
    , fl_soloPos :: !Int -- ^ Position of first character in partial line
    , fl_metric :: !LayoutMetric
    }
    -- | A fragment with internal line breaks.
  | FragmentLines
    { fl_first :: !Text  -- ^ First partial line
    , fl_middle :: ![Text] -- ^ Middle lines
    , fl_last :: !Text  -- ^ Last partial line, not including indentation
    , fl_firstPos :: !Int -- ^ Position of first character in first line
    , fl_lastPos :: !Int  -- ^ Position of first character in last line
    , fl_metric :: !LayoutMetric
    }

-- | Concatenate two fragments
catFragments :: BadnessOptions -> Fragment -> Fragment -> Fragment
catFragments opts x@(FragmentSpan {}) y@(FragmentSpan {}) =
  FragmentSpan
  { fl_solo = fl_solo x <> fl_solo y
  , fl_soloPos = fl_soloPos x
  , fl_metric = addMetrics (fl_metric x) (fl_metric y)
  }

catFragments opts x@(FragmentSpan {}) y@(FragmentLines {}) =
  FragmentLines
  { fl_first = fl_solo x <> fl_first y
  , fl_middle = fl_middle y
  , fl_last = fl_last y
  , fl_firstPos = fl_soloPos x
  , fl_lastPos = fl_lastPos y
  , fl_metric = addMetrics (fl_metric x) (fl_metric y)
  }

catFragments opts x@(FragmentLines {}) y@(FragmentSpan {}) =
  FragmentLines
  { fl_first = fl_first x
  , fl_middle = fl_middle x
  , fl_last = fl_last x <> fl_solo y
  , fl_firstPos = fl_firstPos x
  , fl_lastPos = fl_lastPos x
  , fl_metric = addMetrics (fl_metric x) (fl_metric y)
  }

catFragments opts x@(FragmentLines {}) y@(FragmentLines {}) = let
  newLine = Text.replicate (fl_lastPos x) " " <> fl_last x <> fl_first y
  in FragmentLines
     { fl_first = fl_first x
     , fl_middle = fl_middle x ++ newLine : fl_middle y
     , fl_last = fl_last y
     , fl_firstPos = fl_firstPos x
     , fl_lastPos = fl_lastPos y
     , fl_metric = addMetrics (fl_metric x) (fl_metric y)
     }

-- | Compute the badness of a fragment.  Used for choosing the
--   best of several rendering options.
fragmentBadness :: BadnessOptions -> Fragment -> Int
fragmentBadness opts f = let
  m = fl_metric f
  in explicitOverfill opts (b_overfill m) +
     newlineBadness opts * (b_height m)

-- | Convert a fragment to text
fragmentToText :: Fragment -> Text
fragmentToText (FragmentSpan a pos _) = Text.replicate pos " " <> a

fragmentToText (FragmentLines f m l _ pos _) =
  let lText = Text.replicate pos " " <> l
  in Text.intercalate "\n" (f : m ++ [lText])

-- | A fragment consisting of a partial line of text
textFragment :: BadnessOptions -> Int -> Text -> Fragment
textFragment opts pos text =
  FragmentSpan
  { fl_solo = text
  , fl_soloPos = pos
  , fl_metric = LayoutMetric
                { b_overfill = countOverfill opts pos (Text.length text)
                , b_height = 0
                , b_end = pos + Text.length text
                }
  }

-- | An empty fragment
emptyFragment :: Int -> Fragment
emptyFragment pos =
  FragmentSpan
  { fl_solo = ""
  , fl_soloPos = pos
  , fl_metric = LayoutMetric { b_overfill = 0, b_height = 0, b_end = pos }
  }

-- | A fragment consisting of a line break with the given indentation
lineBreakFragment :: BadnessOptions -> Int -> Int -> Fragment
lineBreakFragment opts pos indent =
  FragmentLines
  { fl_first = ""
  , fl_middle = []
  , fl_last = ""
  , fl_firstPos = pos
  , fl_lastPos = indent
  , fl_metric = LayoutMetric { b_overfill = 0, b_height = 1, b_end = indent }
  }
