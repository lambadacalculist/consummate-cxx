{-# LANGUAGE OverloadedStrings #-}

module Language.ConsummateCXX.Predefined
  (
    -- * Primitive types
    void,
    char,
    uchar,
    short,
    ushort,
    int,
    uint,
    long,
    ulong,
    std_string,
    std_unique_ptr,
    std_shared_ptr
  )
where

import Data.Text (Text)
import Language.ConsummateCXX.Internal.ReqType
import Language.ConsummateCXX.Internal.Syntax

scalar name = value $ unameType name

void, char, uchar, short, ushort, int, uint, long, ulong, std_string :: ReqType
void = ReqType immovableReq (unameType "void")
char = scalar "char"
uchar = scalar "unsigned char"
short = scalar "short"
ushort = scalar "unsigned short"
int = scalar "int"
uint = scalar "uint"
long = scalar "long"
ulong = scalar "unsigned long"
std_string = dataType $ qnameType [name "std"] "string"

std_unique_ptr, std_shared_ptr :: Type -> ReqType
std_unique_ptr t =
  let pt = IDType $ qinst [name "std"] "unique_ptr" [TypeArg t]
  in resource pt

std_shared_ptr t =
  let pt = IDType $ qinst [name "std"] "shared_ptr" [TypeArg t]
  in dataType pt
