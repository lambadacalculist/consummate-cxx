
{-# LANGUAGE OverloadedStrings #-}
module Language.ConsummateCXX
  (
    module Language.ConsummateCXX.ADT,
    module Language.ConsummateCXX.Predefined,

    -- * Constructing types
    unameType,
    resource,
    value,
    dataType,

    -- * Generating code
    forwardDeclareADT,
    declareADT,
    fullDeclareADTs,
    insertIntoText,
    insertIntoFile,
  )
where

import qualified Data.Text as Text
import Data.Text(Text)
import qualified Data.Text.Lazy as TextLazy
import qualified Data.Text.Lazy.IO as TextLazy

import Language.ConsummateCXX.ADT
import Language.ConsummateCXX.Internal.ReqType
import Language.ConsummateCXX.Internal.Syntax
import Language.ConsummateCXX.Predefined
import Language.ConsummateCXX.CodeGeneration
import Language.ConsummateCXX.Internal.Layout as Layout
import Language.ConsummateCXX.Internal.Render as Render

-- | Creates header file code with forward declarations an ADT.
--   This code declares but doesn't define the ADT's classes.
forwardDeclareADT :: ADT -> Text
forwardDeclareADT adt =
  Layout.render $ Layout.vcat $ map Render.renderDeclSemi $
  forwardDeclaration adt

-- | Creates header file code that declares an ADT.
--   The declaration must be after a forward declaration of the same ADT,
--   otherwise it may fail to compile.
--
--   The generated code should be placed at a top-level scope.
--   It may be concatenated with other code.
declareADT :: ADT -> Text
declareADT adt =
  Layout.render $ Layout.vcat $ map Render.renderDeclSemi $ declaration adt

-- | Creates header file code that forward declares, then declares some ADTs.
fullDeclareADTs :: [ADT] -> Text
fullDeclareADTs adts =
  Text.intercalate "\n" $ map forwardDeclareADT adts ++ map declareADT adts

-- | Replaces text in a text string.
--
--   This function is provided for inserting generated code into a
--   C++ file.
insertIntoText :: [(Text, Text)] -- ^ (text, replacement) substitutions
               -> TextLazy.Text  -- ^ Text to replace into
               -> TextLazy.Text  -- ^ Result of replacement
insertIntoText substs text = foldr substitute text substs
  where
    substitute (key, value) text =
      TextLazy.replace (TextLazy.fromStrict key)
      ("\n" <> TextLazy.fromStrict value <> "\n") text

-- | Replaces text in a file.
--
--   This function is provided for inserting generated code into a
--   C++ file.
insertIntoFile :: [(Text, Text)] -- ^ (text, replacement) substitutions
               -> FilePath       -- ^ File to read text from
               -> FilePath       -- ^ File to write result with inserted text
               -> IO ()          -- ^ Performs replacement
insertIntoFile substs srcPath dstPath = do
  srcText <- TextLazy.readFile srcPath
  let dstText = insertIntoText substs srcText
  TextLazy.writeFile dstPath dstText

