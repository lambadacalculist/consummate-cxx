
module CommandLine where

import System.Environment

import Utils

data ParsedArgs = ParsedArgs {
    input :: FilePath
} deriving(Show)

parseArgs :: IO ParsedArgs
parseArgs = do
    args <- getArgs
    case args of
        [path] -> return $ ParsedArgs path
        _ -> failAndExit "Expecting input file path on command line"
