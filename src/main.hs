

{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as Text
import Data.Text(Text)
import qualified Data.Text.IO as Text.IO

import Language.ConsummateCXX
import Language.ConsummateCXX.Internal.Parser

import CommandLine(ParsedArgs(..), parseArgs)
import Utils
import Debug.Trace
import Data.Text.Prettyprint.Doc (Pretty(pretty))

transformFileContents :: FilePath -> Text -> Either String Text
transformFileContents path contents = do
    chunks <- parseFile path contents
    texts <- mapM transformChunk chunks
    return $ Text.concat texts

transformChunk :: ParsedChunk -> Either String Text
transformChunk (Uninterpreted t) = return t
transformChunk (ParsedDeclaration adt) =
    let fwd = forwardDeclareADT adt
        dcl = declareADT adt
        in return $ fwd <> "\n" <> dcl

main = do
    args <- parseArgs
    inputText <- Text.IO.readFile $ input args
    outputText <- case transformFileContents (input args) inputText of
        Left err -> failAndExit err
        Right x -> return x
    Text.IO.putStrLn outputText
    return ()
