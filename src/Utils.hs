
module Utils where

import System.Exit
import System.IO

failAndExit :: String -> IO a
failAndExit message = do
    hPutStrLn stderr ("error: " ++ message)
    exitFailure
