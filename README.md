
Consummate C++
==============

Have you ever looked at C++ and thought "This language would be
perfect if only it had algebraic data types"?  Of course you haven't.
But you get algebraic data types anyway.
**Con**struct **sum** types with Consummate C++.

Consummate C++ is a C++ preprocessor that turns concise algebraic
data type definitions into C++ code.  It is an experimental hobby project
and you should use it.  For example, you define a data type like this:

    adt class Tree {
        Leaf(int tag);
        Node(std::unique_ptr<Tree> left, std::unique_ptr<Tree> right);
    };

And then use it like this:

    int sum(const Tree &t) {
      return t.match(
        [](const Leaf &l) { return l.get_tag(); },
        [](const Node &n) { return sum(*n.ref_left() + sum(*n.ref_right())) });
    }

Read below for a more thorough explanation.  Or, skip ahead to
[Installation](#installation) or [User Guide](#guide).

Sum types
---------

This is a brief introduction to sum types.  If you have used a
language with sum types before, such as Rust, Haskell, OCaml, Scala,
or Swift, this will be familiar to you.

Consider a data type of arithmetic expressions like `1 + (2 * 4)`.
Every expression is either a value like `1` or a term like `2 * 4`.
This is a sum type because every expression is one of a predetermined
set of variants--either a value or a term.  We could define a struct
for this data type as below.

    enum class Operator { ADD, MUL };
 
    struct Expr {
      bool is_int;
      int value;        // Integer value if is_int = 1
      Operator oper;    // Operator if is_int = 0
      Expr *left;       // Left operand if is_int = 0
      Expr *right;      // Right operand if is_int = 0
    };

For example,
`new Expr{0, 0, Operator::ADD, new Expr{1, 10}, new Expr{1, 10}}`
creates objects representing the expression `10 + 10`.

This basic solution isn't good C++ style, though.  It's easy to
inadvertently confuse values and terms, thereby using fields that
don't contain meaningful data or mismanaging memory referenced by
the pointers in the struct.  Better C++ style would:

- Use smart pointers such as `unique_ptr` to manage memory
- Use move semantics to minimize copying
- Use constructors and destructors to maintain correctness invariants
- Enforce const correctness of member access
- Provide a visitor interface for examining expressions

Writing all this is tedious even for experts, and downright
bewildering if you're just emerging from the dark ages of OOP into
the heady togas and crystals of C++17.  But take heart, I'm here
to guide you.

Defining a data type
--------------------

Let's use Consummate C++ to define expressions like we mean it.
Here is a walkthrough of the example code in `examples/Expr/`.
The example's data type declaration is in this header file:

    // Expr.h.in
    #ifndef EXPR_H
    #define EXPR_H

    #include <utility>
    #include <memory>

    enum class Operator { ADD, MUL };

    adt class Expr {
      Value(int value);
      Term([[value]] Operator oper,
           std::unique_ptr<Term> left,
           std::unique_ptr<Term> right);
    };
    
    #endif

An algebraic data type definition begins with `adt class` and the
name of the class being defined (`Expr`).  The body of a data type
definition defines constructors (`Value` and `Term`).  Although they
look like function declarations, they are actually declaring the
constructors' fields.  Consummate C++ needs to know how to access
fields: for example, `unique_ptr` cannot be copied, so Consummate
C++ will not generate a getter function for it.  The C++ attribute
`[[value]]` tells Consummate C++ to treat `oper` as a value type,
that is, one that should be passed around by copying.  The other
types are recognized automatically, so no attribute is needed.

Once you have installed the preprocessor, run it with
`consummate-cxx Expr.h.in > Expr.h`.  It turns the data type
definition into classes and functions, which the C++ compiler can
compile.

Using a data type
-----------------

The generated code defines ways of creating objects, discriminating
between variants, and accessing fields.  For this example, let's take
a look at how to construct and evaluate terms.

To create objects, call the generated constructor function for a
variant.  Constructor functions are based on the variant's name, and
their parameters are the variant's field values.  For example,
`make_Term(Operator::MUL, make_Value(2), make_Value(4))` creates an
expression `2 * 4`.  These functions return `unique_ptr<Expr>`.

To access an `Expr`, the first step is to decide whether it is
a `Value` or a `Term` by calling its `match` method.  Provide a
lambda for handling each variant.  For example, the following
function returns 0 for values and 1 for terms:

    int eval (const Expr &e) {
      return e.match(
        [](const Value &v) { return 0 /* TODO */; },
        [](const Term &t) { return 1 /* TODO */; });
    }

Once you have a `Value` or `Term`, you can call methods to
access its fields.  A `Value` is evaluated by calling
`get_value` to get and return its value.

    int eval (const Expr &e) {
      return e.match(
        [](const Value &v) {
          return v.get_value();
        },

Subexpressions use the move-only type `unique_ptr<Expr>`,
so they have to be accessed in a different way.
Instead of getting a copy of the `left` and `right` fields,
we get references to them by calling `ref_left`.
Using that, we can evaluate terms.

        [](const Term &t) {
          int l = eval(*t.ref_left());
          int r = eval(*t.ref_right());
          switch(t.get_oper()) {
          case Operator::ADD:
            return l + r;
          case Operator::MUL:
            return l * r;
          };
        });
    }


<a name='installation'></a>

Installation
============

Before installing, you need to have [Stack](http://www.haskellstack.org).

Get a copy of the Consummate C++ source tree from the main repository
at https://gitlab.com/lambadacalculist/consummate-cxx .
In the root directory, run `stack install`.
It will build and install the `consummate-cxx` program.


<a name='guide'></a>

User Guide
==========

The recommended way of using `consummate-cxx` is to run it as a
preprocessor on header files when building your C++ program.  It
takes an input filename on the command line and it writes output to
standard output.  Here is an example.

    consummate-cxx include/foo.h > build/include/foo.h

Syntax
------

Consummate C++ preprocesses algebraic data type declarations.
A data type declaration looks like this.

    adt class <class-name> {
      <constructor-declaration-list>
    };

A constructor declaration is written like a function declaration
with no return type.

    <constructor-name>(<parameter-list>);
    
The parameter list declares the types of the constructor's fields.
Parameters can have a `[[value]]`, `[[movable]]`, or `[[copyable]]`
attribute to control how the parameter is accessed, as described in
[Copyable and movable data](#copymove).

What is generated
-----------------

Each `adt class` declaration produces the following things.

*  A class definition of the ADT class, `<class-name>`.
*  A class definition of each constructor `<constructor-name>`.
   These are subclasses of their ADT class.
*  Lifecycle methods for each constructor: construction from fields,
   copy constructor, move constructor, copy assignment, move assignment,
   and destructor.  Methods are only generated if all fields of all
   constructors are [compatible](#copymove) with it.
*  Accessor methods for each field of each constructor: `get_<field-name>`,
   `set_<field-name>`, `take_<field-name>`, `put_<field-name>`,
   and `ref_<field-name>`.  Get and set methods are only created
   for copyable fields.  Take and put methods are only created for
   movable fields.
*  A factory function `make_<constructor-name>` for each constructor.
*  A match function for the ADT class.  It is overloaded for const and
   non-const references.

<a name='copymove'></a>

Copyable and movable data
-------------------------

In C++, you have a choice of copying data, moving data, or using
indirection such as pointers.  Data types are designed (one hopes)
to be manipulated in one of these ways.  Data types can be both
copyable and movable.  Consummate C++ generates code according to
whether a data type is copyable and/or movable, and which is
preferred if both are supported.

Consummate C++ decides which protocol is supported
according to the following rules:

*  Pointers and primitive integer and floating-point types are
   *value types*.  They are copyable and movable, and
   copying is preferred.
*  `std::unique_ptr` is movable and not copyable.
*  Other types are assumed to be copyable and movable, and
   moving is preferred, unless this is overridden with an attribute.
*  A declaration annotated with `[[value]]` is treated as a value
   type, like primitive integer types.
*  A declaration annotated with `[[movable]]` is treated as
   move-only, like `std::unique_ptr`.
*  A declaration annotated with `[[copyable]]` is treated as
   copy-only.

Annotations are used on constructor field declarations, as in
`Term([[value]] Operator oper);`.

Consummate C++ only recognizes the full name of a type, such as
`int` or `std::unique_ptr`.  It does not recognize equivalent types
that were defined using a typedef or using statement.  You can
always override the behavior with an annotation.

Data that is neither copyable nor movable can't be used in
an ADT.  Consider using `std::unique_ptr`, `std::shared_ptr`, or a
raw pointer instead.

Consummate C++ generates code according to which protocols are
supported:

* If moving is supported, it creates `take_<field-name>` and
  `put_<field-name>` methods to read and write the field.
* If copying is supported, it creates `get_<field_name>` and
  `put_<field_name>` methods to read and write the field.
* It creates a `ref_<field_name>` field to access the field by
  reference.
* If moving is preferred, the constructor takes an rvalue
  reference
  
Here is an example of the above.  The following defines one
adt class with one field.  Since the field's type is
unrecognized and it has no annotation, it is copyable and movable,
and moving is preferred.

    adt class Cls {
      Con(T f);
    };

Consummate C++ generates the following methods for accessing T.

    T &&take_f();
    void put_f(T &&);
    T get_f() const;
    void set_f(T);
    T &ref_f();
    const T &ref_f() const;

The constructor takes a `T` parameter regardless.  Since moving
is preferred, the parameter will be moved into the class.

    std::unique_ptr<Cls> make_Con(T f);

For each constructor, a copy constructor and copy assignment
operator are generated if all fields are copyable.  Likewise, a
move constructor and move assignment operator are generated if
all fields are movable.

Limitations of the parser
-------------------------

The preprocessor does not deal with all aspects of C++ syntax, so it can
fail on some kinds of code.

*  Anything that begins with `adt class` gets processed.  That includes
   contents of strings and comments.  You can't hide code with `#if 0`.
   Consummate C++ sees all.

*  Consummate C++ does not keep track of the types that were defined,
   so it can't remember information about them that could help with
   code generation.  It doesn't recognize types that were renamed with
   typedef or using.

*  Expression syntax is not recognized in template parameters.
   For example, `std::array<int, 3>` will result in a parse error.

*  Parse errors are sometimes unhelpful.
