
#include <memory>
#include <string>

class Expr;
class Var;

using ExprPtr = std::unique_ptr<Expr>;

class Expr {
public:
  virtual ~Expr() {}
  ExprPtr clone() const;
};

class Var : public Expr {
private:
  std::string str;

public:
  Var(std::string str_) : str(str_) {}
  Var(Var &&other) = delete;
  Var(const Var &other) = delete;
  const Var &operator=(const Var &other) = delete;
  const Var &operator=(Var &&other) = delete;

  const std::string &getStr() const { return str; }
  std::string takeStr() { return std::move(str); }
  void putStr(std::string str_) { str = std::move(str_); }
};

ExprPtr makeVar(std::string str_) {
  return std::make_unique<Var>(std::move(str_));
}

template<typename F_Var,
         typename R = decltype(std::declval<const F_Var &>()
                               (std::declval<const Var &>()))>
R match(const Expr &e, const F_Var &f_Var) {
  if (const Var *v = dynamic_cast<const Var *>(&e)) {
    return f_Var(*v);
  }
  throw std::bad_cast();
}
