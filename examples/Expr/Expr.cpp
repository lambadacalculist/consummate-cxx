// Expr.cpp
// An example program using generated data types
#include "Expr.h"

#include <iostream>

// Evaluate expression 'e'
int eval(const Expr &e) {
  return e.match(
    [](const Value &v) {
      return v.get_value();
    },
    [](const Term &t) {
      int l = eval(*t.ref_left());
      int r = eval(*t.ref_right());
      switch(t.get_oper()) {
      case Operator::ADD:
        return l + r;
      case Operator::MUL:
        return l * r;
      };
    });
}

int main(int argc, char **argv) {
  // Construct an expression representing 1 + (2 * 4)
  auto t = make_Term(Operator::ADD,
                     make_Value(1),
                     make_Term(Operator::MUL,
                               make_Value(2),
                               make_Value(4)));

  // Evaluate the expression and print the result
  std::cout << eval(*t) << std::endl;
  return 0;
}
