
{-# LANGUAGE OverloadedStrings #-}
module Execute(testExecute) where

import Test.HUnit

import Language.ConsummateCXX.ADT
import Language.ConsummateCXX.Internal.ReqType
import TestUtil

testExecute = TestList
              [ testHeader
              ]

-- Test code generation for a simple data type
testHeader =
  let adt = ADT [] "C" uniquePtr resourceReq ["X" =:: []]
  in testCompilable [adt]
