
{-# LANGUAGE OverloadedStrings #-}
module CodeGeneration(testCodeGeneration) where

import Data.Char
import Test.HUnit
import System.Exit

import Language.ConsummateCXX.ADT
import Language.ConsummateCXX.CodeGeneration
import Language.ConsummateCXX.Internal.ReqType
import Language.ConsummateCXX.Internal.Render
import Language.ConsummateCXX.Internal.Syntax
import Language.ConsummateCXX.Predefined

import TestUtil

testCodeGeneration = TestList
                     [ testClass
                     , testMatcher
                     , testConstMatcher
                     ]

expr = ADT [] "Expr" uniquePtr dataTypeReq [varE, litE]
  where
    varE = Constructor "VarE" [Field "name" std_string]
    litE = Constructor "LitE" [Field "value" int]

testMatcher = renderDeclSemi (matcher False True expr) `testRNS`
  "template<typename F_VarE, typename F_LitE> \
  \decltype(std::declval<F_VarE>()(std::declval<VarE>())) \
  \Expr::match(const F_VarE &f_VarE, const F_LitE &f_LitE) { \
  \  if (VarE *p = dynamic_cast<VarE *>(this)) \
  \    return f_VarE(*p); \
  \  else if (LitE *p = dynamic_cast<LitE *>(this)) \
  \    return f_LitE(*p); \
  \  else throw std::bad_cast(); \
  \}"

testConstMatcher = renderDeclSemi (matcher True True expr) `testRNS`
  "template<typename F_VarE, typename F_LitE> \
  \decltype(std::declval<F_VarE>()(std::declval<const VarE>())) \
  \Expr::match(const F_VarE &f_VarE, const F_LitE &f_LitE) const { \
  \  if (const VarE *p = dynamic_cast<const VarE *>(this)) \
  \    return f_VarE(*p); \
  \  else if (const LitE *p = dynamic_cast<const LitE *>(this)) \
  \    return f_LitE(*p); \
  \  else throw std::bad_cast(); \
  \}"

testClass =
  let con = Constructor "C" [Field "x" int, Field "y" std_string]
      adt = ADT [] "B" uniquePtr dataTypeReq [con]
  in renderModule (Module (constructorDeclaration adt con)) `testRNS`
     "class C : public B { \
     \private: int x_; std::string y_; \
     \public: \
     \C(int x, std::string y) : x_(x), y_(std::move(y)) {} \
     \C(const C &other) : x_(other.x_), y_(other.y_) {} \
     \C(C &&other) : x_(other.x_), y_(std::move(other.y_)) {} \
     \C &operator=(const C &other) { x_ = other.x_; y_ = other.y_; return *this; } \
     \C &operator=(C &&other) { x_ = other.x_; y_ = std::move(other.y_); return *this; } \
     \int get_x() const { return x_; } \
     \void set_x(int x) { x_ = x; } \
     \std::string &ref_y() { return y_; } \
     \const std::string &ref_y() const { return y_; } \
     \std::string take_y() { return std::move(y_); } \
     \void put_y(std::string y) { y_ = std::move(y); } \
     \}; \
     \std::unique_ptr<B> make_C(int x, std::string y) { \
     \return std::unique_ptr<B>(new C(x, std::move(y))); \
     \}"
