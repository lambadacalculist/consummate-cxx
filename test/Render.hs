
{-# LANGUAGE OverloadedStrings #-}
module Render(testRender) where

import Data.Char
import Test.HUnit
import System.Exit

import Language.ConsummateCXX.Internal.Syntax
import Language.ConsummateCXX.Internal.Render
import Language.ConsummateCXX.Predefined

import TestUtil

testRender = TestList
             [ testFunDef
             , testDestructorDef
             , testClassDef
             ]

testFunDef =
  renderDeclSemi (FDecl $ funDef (uname "foo") [] [varDecl (uname "x") int] void [] []) `testRNS`
  "void foo(int x) {}"

testDestructorDef =
  let decl =
        FDecl $ FunDecl (uname "~Foo") ["virtual"] [] Nothing [] [] (Just [])
  in renderDeclSemi decl `testRNS` "virtual ~Foo() {}"

testClassDef =
  renderDeclSemi d `testRNS` "class S : public B { std::string member; S(int x) : member(x) {} };"
  where
    d = CDecl $ ClassDecl (uname "S") (Just $ uname "B")
        [ VDecl $ varDecl (uname "member") (qnameType [name "std"] "string")
        , FDecl (constructorFunDef (uname "S") []
                 [varDecl (uname "x") int] [] [("member", unameE "x")] [])
        ]
