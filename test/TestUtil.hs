
{-# LANGUAGE OverloadedStrings, BangPatterns #-}
module TestUtil where

import Control.Exception
import Data.Char
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import Test.HUnit
import System.FilePath
import qualified System.Process as Process
import System.Directory
import System.Exit
import System.IO

import Language.ConsummateCXX
import Language.ConsummateCXX.ADT
import Language.ConsummateCXX.Internal.Syntax
import Language.ConsummateCXX.Internal.Layout
import Language.ConsummateCXX.Internal.Render
import Language.ConsummateCXX.Predefined

withTempFile :: String -> ((FilePath, Handle) -> IO a) -> IO a
withTempFile template f = do
  tmpDir <- getTemporaryDirectory
  bracket (openTempFile tmpDir template) cleanupFile f
  where
    cleanupFile (path, handle) = do
      hClose handle
      removeFile path

-- | Test whether a header file can be compiled.
testCompilable :: [ADT] -> Test
testCompilable adts = TestCase $ do
  withTempFile "test.cpp" $ \(path, hdl) -> do
    withCurrentDirectory (dropFileName path) $ do
      Text.hPutStr hdl fileText
      hClose hdl
      process <- Process.spawnProcess "g++"
                 ["-std=c++11", "-Werror", "-c", path]
      ec <- Process.waitForProcess process
      case ec of
        ExitSuccess -> return ()
        ExitFailure _ -> do
          putStrLn "File text:"
          Text.putStrLn fileText
          assertFailure "Cannot compile generated code with g++"
  where
    generatedCode = Text.concat $ map forwardDeclareADT adts ++ map declareADT adts
    fileText = "#include <memory>\n" <> generatedCode


-- | Test whether a printable object renders to the expected text.
--   Whitespace differences are ignored by condensing any whitespace
--   sequence to a single space.
testRNS :: Render -> String -> Test
infix 1 `testRNS`
printable `testRNS` expected =
  let expectedText = normalizeWhitespace expected
      computedText = normalizeWhitespace $ Text.unpack $ render printable
  in TestCase $
     case findDifference expectedText computedText
     of Nothing -> pure ()
        Just (i, e, c) ->
          let message = "expected: " ++ expectedText ++ "\n" ++
                        "got:      " ++ computedText ++ "\n" ++
                        "First difference at " ++ show i ++ ": " ++ e ++ "vs. " ++ c
          in assertFailure message
  where
    findDifference xs ys = go 0 xs ys
      where
        go (!i) (x:xs) (y:ys) | x == y = go (i+1) xs ys
                              | otherwise = Just (i, show x, show y)
        go i (x:_) [] = Just (i, show x, "end of string")
        go i [] (y:_) = Just (i, "end of string", show y)
        go i [] [] = Nothing
        
normalizeWhitespace :: String -> String
normalizeWhitespace cs =
  let stripped = reverse $ dropWhile isSpace $ reverse $ dropWhile isSpace cs
  in go stripped
  where
    go (c:cs)
      | isSpace c = ' ' : go (dropWhile isSpace cs)
      | otherwise = c : go cs

    go [] = []
