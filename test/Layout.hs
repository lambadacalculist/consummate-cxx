
{-# LANGUAGE OverloadedStrings #-}
module Layout(testLayout) where

import Test.HUnit

import Language.ConsummateCXX.Internal.Layout
import Language.ConsummateCXX.Internal.Layout.Badness
import TestUtil

testLayout = TestList
             [ testSplit1, testSplit2, testSplit3,
               testSplit4, testSplit5
             ]

narrowOpts = defaultBadnessOptions { bs_ribbonWidth = 24 }

c2 = token "ab"
c4 = token "abcd"
c5 = token "abcde"
c6 = token "abcdef"
c7 = token "abcdefg"
c8 = token "abcdefgh"
c12 = token "abcdefghijkl"

axsep xs = align $ xsep xs

-- Line-splitting optimization tests.  These tests have the same
-- structure and different element lengths, so the best split is
-- in one of 3 different places.
testSplit1 =
  renderOpts narrowOpts (axsep [c8, axsep [c5, axsep [c4, c4, c4]]])
  ~?= "abcdefgh\n\
      \abcde abcd abcd abcd"

testSplit2 =
  renderOpts narrowOpts (axsep [c5, axsep [c8, axsep [c5, c5, c5]]])
  ~?= "abcde abcdefgh\n\
      \      abcde abcde abcde"

testSplit3 =
  renderOpts narrowOpts (axsep [c2, axsep [c2, axsep [c7, c7, c7]]])
  ~?= "ab ab abcdefg\n\
      \      abcdefg\n\
      \      abcdefg"

funcall f xs = f <> alternatives [single, multi]
  where
    args = punctuate (token ",") xs
    single = token "(" <> hsep args <> token ")"
    multi = token "(" <> nest 4 (newline <> vcat args) <> newline <> token ")"

testSplit4 =
  renderOpts narrowOpts (funcall c5 [funcall c5 [c6, c6]])
  ~?= "abcde(abcde(\n\
      \    abcdef,\n\
      \    abcdef\n\
      \))"

testSplit5 =
  renderOpts narrowOpts (funcall c5 [funcall c4 [c6, c6]])
  ~?= "abcde(\n\
      \    abcd(abcdef, abcdef)\n\
      \)"
