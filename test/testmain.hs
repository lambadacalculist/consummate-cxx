
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.Char
import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.String
import Test.HUnit
import System.Exit

import Render
import CodeGeneration
import Execute
import Layout

main = do
  counts <- runTestTT $ TestList
            [ testRender
            , testCodeGeneration
            , testExecute
            , testLayout
            ]
  if errors counts > 0 || failures counts > 0
    then exitFailure
    else exitSuccess

